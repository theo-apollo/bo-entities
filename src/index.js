'use strict';

const UserEntities = require('UserEntities');
const UserProfileEntities = require('UserProfileEntities');

module.exports = {
  UserEntities: UserEntities,
  UserProfileEntities: UserProfileEntities
};